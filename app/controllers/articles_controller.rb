class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit, :update, :destroy]

  def index
    @articles = Article.all
    # add in later w/ will_paginate: .paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: "The article was created succesfully." }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: "The article was updated succesfully." }
      else
        format.html { render :new }
      end
    end
  end

  def edit
  end

  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to airticles_url, notice: "The article was succesfully deleted." }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow trusted parameters
    def article_params
      params.require(:article).permit(:title, :text, :slug, :description, :published)
    end
end