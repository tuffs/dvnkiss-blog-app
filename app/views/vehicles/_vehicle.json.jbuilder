json.extract! vehicle, :id, :make, :model, :year, :price, :for_sale, :interior_color, :exterior_color, :craigslist_url, :website_url, :created_at, :updated_at
json.url vehicle_url(vehicle, format: :json)
