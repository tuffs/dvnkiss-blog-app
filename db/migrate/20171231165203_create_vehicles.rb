class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
      t.string :make
      t.string :model
      t.integer :year
      t.integer :price
      t.boolean :for_sale
      t.string :interior_color
      t.string :exterior_color
      t.string :craigslist_url
      t.string :website_url

      t.timestamps
    end
  end
end
