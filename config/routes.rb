Rails.application.routes.draw do
  resources :articles
  devise_for :users
	root to: 'pages#index'
end
